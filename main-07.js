

window.addEventListener("load", function(event) {

  

      ///////////////////
    //// Funtzioak////
  ///////////////////

  var keyDownUp = function(event) {

    controller.keyDownUp(event.type, event.keyCode);

  };

  var resize = function(event) {

    display.resize(document.documentElement.clientWidth - 32, document.documentElement.clientHeight - 32, game.world.height / game.world.width);
    display.render();

  };

  var render = function() {

    display.drawMap(game.world.map, game.world.columns);
    display.drawPlayer(game.world.player, game.world.player.color1);

    p.innerHTML = "SCORE: " + game.world.score;


    display.render();

  };

  var update = function() {

    if (controller.left.active)  { game.world.player.moveLeft();}
    if (controller.right.active) { game.world.player.moveRight(); }
    if (controller.up.active)    { game.world.player.jump(); controller.up.active = false; }

    game.update();

  };


  
                                        

      /////////////////
    //// Objetuak ////
  /////////////////

  var controller = new Controller();
  var display    = new Display(document.querySelector("canvas"));
  var game       = new Game();
  var engine     = new Engine(1000/30, render, update);
  var p= document.createElement("p");
   p.setAttribute("style", "color:white; font-size:2.0em; position:fixed;");
  p.innerHTML = "0";
  document.body.appendChild(p);

      ////////////////////
    //// Hasieratu ////
  ////////////////////

  display.buffer.canvas.height = game.world.height;
  display.buffer.canvas.width = game.world.width;

  display.tile_sheet.image.addEventListener("load", function(event) {

    resize();

    engine.start();

  }, { once:true });

  display.tile_sheet.image.src = "diseñomapa.png";

  window.addEventListener("keydown", keyDownUp);
  window.addEventListener("keyup",   keyDownUp);
  window.addEventListener("resize",  resize);

});