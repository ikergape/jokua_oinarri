

const Engine = function(time_step, update, render) {

  this.accumulated_time        = 0;// azkeneko updatetik akumulatzen den denbora
  this.animation_frame_request = undefined,
  this.time                    = undefined,
  this.time_step               = time_step,// 1000/30 = 30 frame segunduko

  this.updated = false;

  this.update = update;// update funtzioa
  this.render = render;// render funtzioa

  this.run = function(time_stamp) {// Jokuaren buklearen zikloa

    this.accumulated_time += time_stamp - this.time;
    this.time = time_stamp;

    

    //on load erabili ordez ordenagailuak ez badu ondo hartzen jokua krasheatzea saihesten du.
    if (this.accumulated_time >= this.time_step * 3) {

      this.accumulated_time = this.time_step;

    }

  
    while(this.accumulated_time >= this.time_step) {

      this.accumulated_time -= this.time_step;

      this.update(time_stamp);

      this.updated = true;// update a egin badu berriz ere marraztu behar du.

    }

    /* bakarrik updatea egindakoan marrazten du honekin */
    if (this.updated) {

      this.updated = false;
      this.render(time_stamp);

    }

    this.animation_frame_request = window.requestAnimationFrame(this.handleRun);

  };

  this.handleRun = (time_step) => { this.run(time_step); };

};

Engine.prototype = {

  constructor:Engine,

  start:function() {

    this.accumulated_time = this.time_step;
    this.time = window.performance.now();
    this.animation_frame_request = window.requestAnimationFrame(this.handleRun);

  },

  stop:function() { window.cancelAnimationFrame(this.animation_frame_request); }

};

